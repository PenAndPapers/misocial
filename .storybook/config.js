import Vue from 'vue'
import { configure } from '@storybook/vue'
import '../src/tailwind/style.css'

// The code below loads all elements in a global context
const requireComponent = require.context('@elements', true, /[A-Z]\w+\.(vue|js)$/)
requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)
  // Get PascalCase name of component
  const componentName = fileName.split('/').pop().replace(/\.\w+$/, '')
  // Register component globally
  Vue.component(componentName, componentConfig.default || componentConfig)
})

configure(require.context('../src/stories', true, /\.stories\.js$/), module)