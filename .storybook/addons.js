import { addons } from '@storybook/addons';
import '@storybook/addon-actions/register';
import '@storybook/addon-console';
import '@storybook/addon-knobs/register';
import '@storybook/addon-links/register';
import '@storybook/addon-notes/register';
import '@storybook/addon-storysource/register';
import '@storybook/addon-viewport/register';
import theme from './theme';

addons.setConfig({
  theme: theme,
})