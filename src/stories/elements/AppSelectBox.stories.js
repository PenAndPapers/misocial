import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppSelectBox from '@elements/AppSelectBox'

storiesOf('Elements', module)
  .addDecorator(withKnobs)
  .add('AppSelectBox', () => ({
    components: { AppSelectBox },
    template: `<div class="p-4">
      <AppSelectBox @input="action" />
    </div>`,
    methods: { action: action('clicked') }
  }))
