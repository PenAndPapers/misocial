import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppIcon from '@elements/AppIcon'

storiesOf('Elements', module)
  .addDecorator(withKnobs)
  .add('AppIcon', () => ({
    components: { AppIcon },
    template: '<AppIcon class="lnr-chevron-down" />',
    methods: { action: action('clicked') }
  }))
