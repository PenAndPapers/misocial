import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppTextBox from '@elements/AppTextBox'

storiesOf('Elements | AppTextBox', module)
  .addDecorator(withKnobs)
  .add('Default', () => ({
    components: { AppTextBox },
    template: `<div class="p-4">
      <AppTextBox placeholder="First Name" @input="action" />
    </div>`,
    methods: { action: action('clicked') }
  }))
  .add('Error', () => ({
    components: { AppTextBox },
    template: `<div class="p-4">
      <AppTextBox class="border-red-600" placeholder="First Name" />
    </div>`,
    methods: { action: action('clicked') }
  }))
  .add('Success', () => ({
    components: { AppTextBox },
    template: `<div class="p-4">
      <AppTextBox class="border-green-600" placeholder="First Name" />
    </div>`,
    methods: { action: action('clicked') }
  }))
