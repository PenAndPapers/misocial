import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppButton from '@elements/AppButton'

storiesOf('Elements', module)
  .addDecorator(withKnobs)
  .add('AppButton', () => ({
    components: { AppButton },
    template: `<div class="p-4">
      <AppButton 
        class="text-darkgray hover:text-white"
        @click="action"
      >
        Get Started
      </AppButton>
    </div>`,
    methods: { action: action('clicked') }
  }))
