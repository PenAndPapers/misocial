import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppCheckBox from '@elements/AppCheckBox'

storiesOf('Elements', module)
  .addDecorator(withKnobs)
  .add('AppCheckBox', () => ({
    components: { AppCheckBox },
    template: '<AppCheckBox />',
    methods: { action: action('clicked') }
  }))
