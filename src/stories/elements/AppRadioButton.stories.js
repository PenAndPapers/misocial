import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppRadioButton from '@elements/AppRadioButton'

storiesOf('Elements', module)
  .addDecorator(withKnobs)
  .add('AppRadioButton', () => ({
    components: { AppRadioButton },
    template: '<AppRadioButton />',
    methods: { action: action('clicked') }
  }))
