import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppLink from '@elements/AppLink'

storiesOf('Elements', module)
  .addDecorator(withKnobs)
  .add('AppLink', () => ({
    components: { AppLink },
    template: '<AppLink />',
    methods: { action: action('clicked') }
  }))
