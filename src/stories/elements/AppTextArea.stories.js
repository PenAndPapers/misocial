import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import AppTextArea from '@elements/AppTextArea'

storiesOf('Elements | AppTextArea', module)
  .addDecorator(withKnobs)
  .add('Default', () => ({
    components: { AppTextArea },
    template: `<div class="p-4">
      <AppTextArea placeholder="Comments/Suggestions" rows="6" @input="action" />
    </div>`,
    methods: { action: action('clicked') }
  }))
  .add('Error', () => ({
    components: { AppTextArea },
    template: `<div class="p-4">
      <AppTextArea class="border-red-600" placeholder="Comments/Suggestions" rows="6" />
    </div>`,
    methods: { action: action('clicked') }
  }))
  .add('Success', () => ({
    components: { AppTextArea },
    template: `<div class="p-4">
      <AppTextArea class="border-green-600" placeholder="Comments/Suggestions" rows="6" />
    </div>`,
    methods: { action: action('clicked') }
  }))
