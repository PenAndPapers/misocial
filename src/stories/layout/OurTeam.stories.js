import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import OurTeam from '@layout/OurTeam'
import TeamMembers from '@data/TeamMembers'

storiesOf('Layout', module)
  .addDecorator(withKnobs)
  .add('OurTeam', () => ({
    components: { OurTeam },
    props: {
      teamMembers: {
        default: object('Team Members', TeamMembers.data)
      }
    },
    template: '<OurTeam :team-members="teamMembers" />',
    methods: { action: action('clicked') }
  }))
