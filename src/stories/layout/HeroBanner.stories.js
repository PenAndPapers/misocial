import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, text, object } from '@storybook/addon-knobs'

import HeroBanner from '@layout/HeroBanner'
import ContentCopy from '@common/ContentCopy'
import Copy from '@data/ContentCopy'

storiesOf('Layout', module)
  .addDecorator(withKnobs)
  .add('HeroBanner', () => ({
    components: { HeroBanner, ContentCopy },
    props: {
      bgImage: {
        default: text('BG Image', 'https://images.unsplash.com/photo-1506422748879-887454f9cdff?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80')
      },
      details: {
        default: object('Copy', Copy.data[2])
      }
    },
    template: `
    <HeroBanner
      class="lg:py-50 py-36 lg:h-auto h-screen"
      :bg-image="bgImage"
    >
      <ContentCopy :details="details" />
    </HeroBanner>`,
    methods: { action: action('clicked') }
  }))
