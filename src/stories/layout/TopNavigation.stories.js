import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'
import StoryRouter from 'storybook-vue-router'

import TopNavigation from '@layout/TopNavigation'
import NavLinks from '@data/Navigation'

storiesOf('Layout', module)
  .addDecorator(StoryRouter())
  .addDecorator(withKnobs)
  .add('Top Navigation', () => ({
    components: { TopNavigation },
    props: {
      navLinks: {
        default: object('Nav Links', NavLinks.data)
      }
    },
    template: `<TopNavigation 
      class="bg-teal py-5 px-4"
      :nav-links="navLinks" 
    />`,
    methods: { action: action('clicked') }
  }))
