import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'
import StoryRouter from 'storybook-vue-router'

import FooterNavigation from '@layout/FooterNavigation'
import NavLinks from '@data/Navigation'

storiesOf('Layout', module)
  .addDecorator(StoryRouter())
  .addDecorator(withKnobs)
  .add('Footer Navigation', () => ({
    components: { FooterNavigation },
    props: {
      navLinks: {
        default: object('Nav Links', NavLinks.data)
      }
    },
    template: `<FooterNavigation 
      class="py-5 px-4"
      :nav-links="navLinks" 
    />`,
    methods: { action: action('clicked') }
  }))
