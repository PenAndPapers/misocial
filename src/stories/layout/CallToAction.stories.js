import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, boolean } from '@storybook/addon-knobs'

import CallToAction from '@layout/CallToAction'

storiesOf('Layout | CallToAction', module)
  .addDecorator(withKnobs)
  .add('No Tablet', () => ({
    components: { CallToAction },
    template: '<CallToAction />',
    methods: { action: action('clicked') }
  }))
  .add('With Tablet', () => ({
    components: { CallToAction },
    props: {
      showTablet: {
        default: boolean('Show Tablet', true)
      }
    },
    template: '<CallToAction :show-tablet="showTablet" />',
    methods: { action: action('clicked') }
  }))
