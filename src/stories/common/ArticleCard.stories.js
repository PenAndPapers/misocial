import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import ArticleCard from '@common/ArticleCard'
import Articles from '@data/Articles'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Article Card', () => ({
    components: { ArticleCard },
    props: {
      article: {
        default: object('Article', Articles.data[0])
      }
    },
    template: `<div class="w-3/12 m-auto my-4 block">
      <ArticleCard :article="article" />
    </div>`,
    methods: { action: action('clicked') }
  }))
