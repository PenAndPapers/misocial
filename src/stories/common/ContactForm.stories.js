import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import ContactForm from '@common/ContactForm'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Contact Form', () => ({
    components: { ContactForm },
    template: '<ContactForm />',
    methods: { action: action('clicked') }
  }))
