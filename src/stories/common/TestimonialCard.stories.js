import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import TestimonialCard from '@common/TestimonialCard'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Testimonial Card', () => ({
    components: { TestimonialCard },
    template: '<TestimonialCard />',
    methods: { action: action('clicked') }
  }))
