import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object, text, number } from '@storybook/addon-knobs'
import StoryRouter from 'storybook-vue-router'

import SectionCard from '@common/SectionCard'
import Copy from '@data/ContentCopy'

storiesOf('Common | Section Card', module)
  .addDecorator(StoryRouter())
  .addDecorator(withKnobs)
  .add('Default Layout', () => ({
    components: { SectionCard },
    props: {
      details: {
        default: object('Copy', Copy.data[2])
      },
      imgSrc: {
        default: text('Image', 'https://gitlab.com/PenAndPapers/misocial/-/raw/master/src/assets/img/rocket.png')
      },
      layoutType: {
        default: number('Layout Type', 1)
      }
    },
    template: '<SectionCard :details="details" :img-src="imgSrc" :layout-type="layoutType" />',
    methods: { action: action('clicked') }
  }))
  .add('Left image layout', () => ({
    components: { SectionCard },
    props: {
      details: {
        default: object('Copy', Copy.data[2])
      },
      imgSrc: {
        default: text('Image', 'https://gitlab.com/PenAndPapers/misocial/-/raw/master/src/assets/img/phone.png')
      },
      layoutType: {
        default: number('Layout Type', 2)
      }
    },
    template: '<SectionCard :details="details" :img-src="imgSrc" :layout-type="layoutType" />',
    methods: { action: action('clicked') }
  }))
