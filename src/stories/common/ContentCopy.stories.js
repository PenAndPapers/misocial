import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'
import StoryRouter from 'storybook-vue-router'

import ContentCopy from '@common/ContentCopy'
import Copy from '@data/ContentCopy'

storiesOf('Common | ContentCopy', module)
  .addDecorator(StoryRouter())
  .addDecorator(withKnobs)
  .add('Left Aligned', () => ({
    components: { ContentCopy },
    props: {
      details: {
        default: object('Copy', Copy.data[0])
      }
    },
    template: `<div class="w-5/12 m-auto my-4 block bg-teal p-10">
      <ContentCopy :details="details" />
    </div>`,
    methods: { action: action('clicked') }
  }))
  .add('Centered', () => ({
    components: { ContentCopy },
    props: {
      details: {
        default: object('Copy', Copy.data[1])
      }
    },
    template: `<div class="w-5/12 m-auto my-4 block bg-teal p-10">
      <ContentCopy :details="details" />
    </div>`,
    methods: { action: action('clicked') }
  }))
  .add('No Buttons', () => ({
    components: { ContentCopy },
    props: {
      details: {
        default: object('Copy', Copy.data[9])
      }
    },
    template: `<div class="w-5/12 m-auto my-4 block bg-teal p-10">
      <ContentCopy :details="details" />
    </div>`,
    methods: { action: action('clicked') }
  }))
