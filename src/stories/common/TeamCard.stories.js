import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import TeamCard from '@common/TeamCard'
import TeamMembers from '@data/TeamMembers'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Team Card', () => ({
    components: { TeamCard },
    props: {
      member: {
        default: object('Team Member', TeamMembers.data[0])
      }
    },
    template: `<div class="w-3/12 m-auto my-4 block">
      <TeamCard :team-member="member" />
    </div>`,
    methods: { action: action('clicked') }
  }))
