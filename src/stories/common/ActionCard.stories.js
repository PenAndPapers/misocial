import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import ActionCard from '@common/ActionCard'
import Action from '@data/Actions'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Action Card', () => ({
    components: { ActionCard },
    props: {
      details: {
        default: object('Action', Action.data[0])
      }
    },
    template: `<div class="w-3/12 m-auto my-4 block bg-purple p-5">
      <ActionCard :details="details" />
    </div>`,
    methods: { action: action('clicked') }
  }))
