import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import GetStartedCard from '@common/GetStartedCard'
import GetStarted from '@data/GetStarted'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Get Started Card', () => ({
    components: { GetStartedCard },
    props: {
      details: {
        default: object('Details', GetStarted.data[0])
      }
    },
    template: `<div class="w-3/12 m-auto my-4 block">
      <GetStartedCard :details="details" />
    </div>`,
    methods: { action: action('clicked') }
  }))
