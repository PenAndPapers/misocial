import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import PlanCard from '@common/PlanCard'
import Plans from '@data/Plans'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Plan Card', () => ({
    components: { PlanCard },
    props: {
      plan: {
        default: object('Plan', Plans.data[0])
      }
    },
    template: `<div class="w-3/12 m-auto my-4 block">
      <PlanCard :details="plan" />
    </div>`,
    methods: { action: action('clicked') }
  }))
