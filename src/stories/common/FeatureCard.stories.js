import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs, object } from '@storybook/addon-knobs'

import FeatureCard from '@common/FeatureCard'
import Features from '@data/Features'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Feature Card', () => ({
    components: { FeatureCard },
    props: {
      feature: {
        default: object('Feature', Features.data[0])
      }
    },
    template: `<div class="w-3/12 m-auto my-4 block bg-purple p-5">
      <FeatureCard :feature="feature" />
    </div>`,
    methods: { action: action('clicked') }
  }))
