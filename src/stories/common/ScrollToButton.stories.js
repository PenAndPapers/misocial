import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import ScrollToButton from '@common/ScrollToButton'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Scroll To Button', () => ({
    components: { ScrollToButton },
    template: `<div class="m-auto my-4 block">
      <ScrollToButton />
    </div>`,
    methods: { action: action('clicked') }
  }))
