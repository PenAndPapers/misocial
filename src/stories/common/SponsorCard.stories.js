import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'
import { withKnobs } from '@storybook/addon-knobs'

import SponsorCard from '@common/SponsorCard'

storiesOf('Common', module)
  .addDecorator(withKnobs)
  .add('Sponsor Card', () => ({
    components: { SponsorCard },
    template: '<SponsorCard />',
    methods: { action: action('clicked') }
  }))
