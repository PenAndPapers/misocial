# misocial

[Project Link](https://penandpapers.gitlab.io/misocial/)
[Project Storybook](https://penandpapers.gitlab.io/misocial/storybook-build)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Misocial
Mass Impressions
[Mass Impressions Link](https://www.massimpressions.com/web-design-blog/misocial-free-psd-template/)
Dropbox
[Dropbox Link](https://www.dropbox.com/sh/3pzi290i4ryvg5t/AAAyxnHpRho5aZhR05ZtUSlEa?dl=0)

### FYI 
This project was made for practicing Vue JS and it's not for sale.
